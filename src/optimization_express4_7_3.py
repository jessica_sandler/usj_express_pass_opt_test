from scipy.optimize import minimize
from helper_functions.helper_functions_for_regression import *
from configparser import ConfigParser
import numpy as np
import sys


config = ConfigParser()
config.read(sys.argv[1])

   
def get_demand4(coef, op_vals_dict, op_vals_dict_name, x):
    op_vals_dict['express4_avg_price'] = x[0]
    op_vals_dict['price_difference_EXP7_EXP4'] = x[1] - x[0]
    return eval(custom_predict(coef_dict=coef, df=op_vals_dict_name))


def get_demand7(coef, op_vals_dict, op_vals_dict_name, x):
    op_vals_dict['express7_avg_price'] = x[1]
    op_vals_dict['price_difference_EXP7_EXP4'] = x[1] - x[0]
    return eval(custom_predict(coef_dict=coef, df=op_vals_dict_name))

def get_demand3(coef, op_vals_dict, op_vals_dict_name, x):
    op_vals_dict['express3_avg_price'] = x[2]
    op_vals_dict['price_difference_EXP4_EXP3'] = x[0] - x[2]
    return eval(custom_predict(coef_dict=coef, df=op_vals_dict_name))


def get_revenue(x):
    demand4 = get_demand4(coef=coef_express4, op_vals_dict=vals_dict_express, op_vals_dict_name='vals_dict_express',
                          x=x)
    demand7 = get_demand7(coef=coef_express7, op_vals_dict=vals_dict_express, op_vals_dict_name='vals_dict_express',
                          x=x)
    demand3 = get_demand3(coef=coef_express3, op_vals_dict=vals_dict_express, op_vals_dict_name='vals_dict_express',
                          x=x)
    revenue = demand4 * x[0] + demand7 * x[1] + demand3 * x[2]
    revenue = (-1 * revenue)
    return revenue

def get_revenue_sans3(x):
    demand4 = get_demand4(coef=coef_express4, op_vals_dict=vals_dict_express, op_vals_dict_name='vals_dict_express',
                          x=x)
    demand7 = get_demand7(coef=coef_express7, op_vals_dict=vals_dict_express, op_vals_dict_name='vals_dict_express',
                          x=x)
    revenue = demand4 * x[0] + demand7 * x[1]
    revenue = (-1 * revenue)
    return revenue

def get_coef(product_name, file_path):
    # TODO: get the latest coefficients
    coef_df = pd.read_csv(file_path)
    coef_df = coef_df[coef_df['product_name'] == product_name]
    coef_dict = convert_df_to_dict(df=coef_df, key_column='variable', value_column='coefficient')
    #todo
    coef_dict['lhs'] = coef_df['lhs'].unique()
    return coef_dict


def capacity_constraints(x):
    if x.size == 3:
        demand4 = get_demand4(coef=coef_express4, op_vals_dict=vals_dict_express, op_vals_dict_name='vals_dict_express',
                              x=x)
        demand7 = get_demand7(coef=coef_express7, op_vals_dict=vals_dict_express, op_vals_dict_name='vals_dict_express',
                              x=x)
        demand3 = get_demand3(coef=coef_express3, op_vals_dict=vals_dict_express, op_vals_dict_name='vals_dict_express',
                              x=x)
        demand_array = np.array([demand4, demand7, demand3])

        capacity = get_capacities(capacity_dict=cap_dict_express)
        capacity_diff = capacity - demand_array
    else:
        capacity_diff =  capacity_constraints_sans3(x)
    return capacity_diff


def get_capacities(capacity_dict):
    capacities = np.array([capacity_dict['express4_capacity'], capacity_dict['express7_capacity'], capacity_dict['express3_capacity']])
    return capacities

def capacity_constraints_sans3(x):
    demand4 = get_demand4(coef=coef_express4, op_vals_dict=vals_dict_express, op_vals_dict_name='vals_dict_express',
                          x=x)
    demand7 = get_demand7(coef=coef_express7, op_vals_dict=vals_dict_express, op_vals_dict_name='vals_dict_express',
                          x=x)

    demand_array = np.array([demand4, demand7])

    capacity = get_capacities_sans3(capacity_dict=cap_dict_express)
    capacity_diff = capacity - demand_array
    return capacity_diff


def get_capacities_sans3(capacity_dict):
    capacities = np.array([capacity_dict['express4_capacity'], capacity_dict['express7_capacity']])
    return capacities

def myround(x):
    round_down = abs(x - (np.round(x/1000)*1000 - 200 ))
    round_up = abs(x - (np.round(x/1000)*1000 + 800))
    print (x)
    if round_down < round_up:
        return (np.round(x/1000)*1000 - 200 )
    else: 
        return (np.round(x/1000)*1000 + 800)

def build_output_df(optimal_prices, current_prices, date):
    df = dict()
    df['date'] = date
    df['optimal_prices_express4'] = optimal_prices[0]
    df['optimal_prices_express7'] = optimal_prices[1]

    df['current_prices_express4'] = current_prices[0]
    df['current_prices_express7'] = current_prices[1]
    

    df['current_demand_express4'] = get_demand4(coef=coef_express4, op_vals_dict=vals_dict_express,
                                                op_vals_dict_name='vals_dict_express',
                                                x=current_prices)

    df['current_demand_express7'] = get_demand7(coef=coef_express7, op_vals_dict=vals_dict_express,
                                                op_vals_dict_name='vals_dict_express',
                                                x=current_prices)


    df['optimal_demand_express4'] = get_demand4(coef=coef_express4, op_vals_dict=vals_dict_express,
                                                op_vals_dict_name='vals_dict_express',
                                                x=optimal_prices)

    df['optimal_demand_express7'] = get_demand7(coef=coef_express7, op_vals_dict=vals_dict_express,
                                                op_vals_dict_name='vals_dict_express',
                                                x=optimal_prices)
    if optimal_prices[2] != 0:
        df['optimal_demand_express3'] = get_demand3(coef=coef_express3, op_vals_dict=vals_dict_express,
                                                    op_vals_dict_name='vals_dict_express',
                                                    x=optimal_prices)
        df['current_demand_express3'] = get_demand3(coef=coef_express3, op_vals_dict=vals_dict_express,
                                                    op_vals_dict_name='vals_dict_express',
                                                    x=current_prices)
        df['optimal_prices_express3'] = optimal_prices[2]
        df['current_prices_express3'] = current_prices[2]
        df['optimal_demand_express3'] = np.where(df['optimal_demand_express3']>0, df['optimal_demand_express3'], df['current_demand_express3'])
        df['optimal_prices_express3'] = np.where(df['optimal_demand_express3']==df['current_demand_express3'], df['current_prices_express3'],
                                        df['optimal_prices_express3'])
    else:
        df['optimal_demand_express3'] = 0
        df['current_demand_express3'] = 0
        df['optimal_prices_express3'] = 0
        df['current_prices_express3'] = 0
        df['optimal_demand_express3'] = 0
        df['optimal_prices_express3'] = 0

    df['optimal_revenue_express4'] = df['optimal_demand_express4'] * df['optimal_prices_express4']
    df['optimal_revenue_express7'] = df['optimal_demand_express7'] * df['optimal_prices_express7']
    df['optimal_revenue_express3'] = df['optimal_demand_express3'] * df['optimal_prices_express3']

    df['current_revenue_express4'] = df['current_demand_express4'] * df['current_prices_express4']
    df['current_revenue_express7'] = df['current_demand_express7'] * df['current_prices_express7']
    df['current_revenue_express3'] = df['current_demand_express3'] * df['current_prices_express3']

    df['current_total_revenue'] = df['current_revenue_express4'] + df['current_revenue_express7'] + df['current_revenue_express3']
    df['optimal_total_revenue'] = df['optimal_revenue_express4'] + df['optimal_revenue_express7'] + df['optimal_revenue_express3']

    df['time_stamp'] = datetime.now()
    df = pd.DataFrame(data=df, index=[0])

    return df


# ------------------------------------------------------- #
# -------------- START OF OPTIMIZATION ------------------ #
# ------------------------------------------------------- #
# reading operational variables and capacities

op_vars_file_path = ast.literal_eval(config.get('input_file_paths', 'op_vars_file_path'))
capacity_data_file_path = ast.literal_eval(config.get('input_file_paths', 'capacity_data_file_path'))
coef_data_file_path = ast.literal_eval(config.get('input_file_paths', 'coef_data_file_path'))
optimal_results_file_path = ast.literal_eval(config.get('output_file_paths', 'optimal_results_file_path'))

op_var_data = pd.read_csv(op_vars_file_path)
capacity_data = pd.read_csv(capacity_data_file_path)

# getting coefficients
coef_express4 = get_coef(product_name='express4', file_path=coef_data_file_path)
coef_express7 = get_coef(product_name='express7', file_path=coef_data_file_path)
coef_express3 = get_coef(product_name='express3', file_path=coef_data_file_path)

# reading properties
price_difference_EXP7_EXP4 = ast.literal_eval(config.get('price_difference', 'price_difference_EXP7_EXP4'))
price_difference_EXP4_EXP3 = ast.literal_eval(config.get('price_difference', 'price_difference_EXP4_EXP3'))
start_date = ast.literal_eval(config.get('dates', 'start_date'))
end_date = ast.literal_eval(config.get('dates', 'end_date'))

# filtering for required dates
filter_op_var = (op_var_data['date'] >= start_date) & (op_var_data['date'] <= end_date)
filter_capacity_data = (op_var_data['date'] >= start_date) & (op_var_data['date'] <= end_date)
op_var_data = op_var_data[filter_op_var]
capacity_data = capacity_data[filter_capacity_data]
date_range = op_var_data['date']

# defining output_df
optimal_results_df = pd.DataFrame()

for i in date_range:
    vals_dict_express = op_var_data[op_var_data['date'] == i].iloc[0].to_dict()
    cap_dict_express = capacity_data[capacity_data['date'] == i].iloc[0].to_dict()
    
    # defining the constraint dictionary
    if cap_dict_express['express3_capacity'] > 0:
        initial_prices = np.array([vals_dict_express['express4_avg_price'], vals_dict_express['express7_avg_price'], vals_dict_express['express3_avg_price']])
        cons = ({'type': 'ineq', 'fun': capacity_constraints},
            {'type': 'ineq', 'fun': lambda x: get_demand3(coef_express3, vals_dict_express, 'vals_dict_express', x)},
            {'type': 'ineq', 'fun': lambda x: get_demand4(coef_express4, vals_dict_express, 'vals_dict_express', x)},
            {'type': 'ineq', 'fun': lambda x: get_demand7(coef_express7, vals_dict_express, 'vals_dict_express', x)},
            {'type': 'ineq', 'fun': lambda x: x[1] - x[0] - price_difference_EXP7_EXP4},
            {'type': 'ineq', 'fun': lambda x: x[0] - x[2] - price_difference_EXP4_EXP3},
            {'type': 'ineq', 'fun': lambda x: x[0]},
            {'type': 'ineq', 'fun': lambda x: x[1]},
            {'type': 'ineq', 'fun': lambda x: x[2]}
        )
        model = minimize(get_revenue,
                 initial_prices,
                 method='SLSQP',
                 constraints=cons,
                 options={'maxiter': 1000, 'ftol': 100})
        print(model.x)
        rounded_opt = [myround(price) for price in model.x]

    else:
        initial_prices = np.array([vals_dict_express['express4_avg_price'], vals_dict_express['express7_avg_price']])
        cons = ({'type': 'ineq', 'fun': capacity_constraints},
            {'type': 'ineq', 'fun': lambda x: get_demand4(coef_express4, vals_dict_express, 'vals_dict_express', x)},
            {'type': 'ineq', 'fun': lambda x: get_demand7(coef_express7, vals_dict_express, 'vals_dict_express', x)},
            {'type': 'ineq', 'fun': lambda x: x[1] - x[0] - price_difference_EXP7_EXP4},
            {'type': 'ineq', 'fun': lambda x: x[0]},
            {'type': 'ineq', 'fun': lambda x: x[1]}
        )

        model = minimize(get_revenue_sans3,
                 initial_prices,
                 method='SLSQP',
                 constraints=cons,
                 options={'maxiter': 1000, 'ftol': 100})
        print(model.x)
        rounded_opt = [myround(price) for price in model.x]
        rounded_opt = np.append(rounded_opt, [0])
        initial_prices = np.append(initial_prices, vals_dict_express['express3_avg_price'])
        print(rounded_opt)
    
    df_cur_iter = build_output_df(optimal_prices=rounded_opt, current_prices=initial_prices, date=i)
    optimal_results_df = optimal_results_df.append(df_cur_iter)

sort_col_names = ['date', 'current_demand_express4', 'current_demand_express7', 'current_demand_express3',
                  'current_prices_express4',
                  'current_prices_express7', 'current_prices_express3','current_revenue_express4',
                  'current_revenue_express7', 'current_revenue_express3',
                  'current_total_revenue', 'optimal_demand_express4', 'optimal_demand_express7', 'optimal_demand_express3',
                  'optimal_prices_express4', 'optimal_prices_express7', 'optimal_prices_express3', 'optimal_revenue_express4',
                  'optimal_revenue_express7', 'optimal_revenue_express3','optimal_total_revenue', 'time_stamp']

optimal_results_df = optimal_results_df[sort_col_names]

optimal_results_df.to_csv(optimal_results_file_path, index=False)
