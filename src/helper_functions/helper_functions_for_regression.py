from __future__ import division
import ast
import re
import numpy as np
import pandas as pd
from datetime import datetime


def transform(var_list, target_transform_list, join_on=""):
    """
        creates a transformations required for the regression model

        Parameters
        -------------------------------------------------------------------------------------------
        var_list: variable list
        target_transform_list: The Target list which transforms the variable
        join_on: delimiter string for .join function
        returns
        -------------------------------------------------------------------------------------------
        string
        """

    x = []
    for i in range(len(var_list)):
        # if target transformation is a list
        if type(target_transform_list[i]) == str:
            x.append(target_transform_list[i] + "(" + var_list[i] + ")")
        # if target transformation is an integer
        elif type(target_transform_list[i]) == int or type(target_transform_list[i]) == float:
            # if the target transformation is raised to power 1
            if target_transform_list[i] == 1:
                x.append(var_list[i])
            else:
                x.append(var_list[i] + '**' + str(target_transform_list[i]))
    x = join_on.join(x)
    return x


def regression_formula(independent_var_dict, dependent_var_dict):
    """
        creates a regression formula for stats model

        Parameters
        -------------------------------------------------------------------------------------------
        independent_var_dict: independent variable dictionary
        dependent_var_dict: dependent_var_dictionary

        returns
        -------------------------------------------------------------------------------------------
        string
        """
    x = []
    for i in independent_var_dict:
        if len(i.split(':')) > 1:
            list1 = i.split(':')
            list2 = independent_var_dict[i]
            x.append(transform(var_list=list1, target_transform_list=list2, join_on=":"))
        elif len(i.split('*')) > 1:
            list1 = i.split('*')
            list2 = independent_var_dict[i]
            x.append(transform(var_list=list1, target_transform_list=list2, join_on="*"))
        elif len(i.split('+')) > 1:
            list1 = i.split('+')
            list2 = independent_var_dict[i]
            x.append(transform(var_list=list1, target_transform_list=list2, join_on="+"))
        else:
            list1 = i.split(':')
            list2 = independent_var_dict[i]
            x.append(transform(var_list=list1, target_transform_list=list2))
    x = " + ".join(x)

    for j in dependent_var_dict:
        list1 = j.split(':')
        list2 = dependent_var_dict[j]
        dependent_var_transformation = transform(var_list=list1, target_transform_list=list2)

    regression_string = dependent_var_transformation + ' ~ ' + x

    return regression_string


######################################################################################################
# ----------------------- functions for custom predict --------------------------------------------- #
######################################################################################################

def multiplied_vals(var_string, df, coef_dict):
    var_name = get_var_name(var_string)
    transformation_name = get_transformation_name(var_string)
    df_name = get_df_name(df)
    coef = coef_dict[var_string]  # this is i
    if transformation_name == 1:
        x = df_name + '[' + '"' + var_name + '"' + ']' + '*' + str(coef)
    else:
        x = transformation_name + '(' + df_name + '[' + '"' + var_name + '"' + ']' + ')' + '*' + str(coef)
    return x


def multiplied_vals_interaction(var_string, i, df, coef_dict):
    var_name = get_var_name(var_string)
    transformation_name = get_transformation_name(var_string)
    df_name = get_df_name(df)
    coef = coef_dict[i]
    if transformation_name == 1:
        # no transformation on the variable
        x = (df_name + '[' + '"' + var_name + '"' + ']')
    else:
        x = (transformation_name + '(' + df_name + '[' + '"' + var_name + '"' + ']' + ')')
    return x, str(coef)


def get_var_name(var_string):
    m = re.search(r"\(([A-Za-z0-9_]+)\)", var_string)
    if m is None:
        var_name = var_string
    elif m is not None:
        var_name = m.group(1)
    return var_name


def get_transformation_name(var_string):
    if "(" in var_string:
        transformation_name = var_string.split("(")[0]
    else:
        transformation_name = 1
    return transformation_name


def get_df_name(df):
    return df


def custom_predict(coef_dict, df, disp=False):
    x = []
    for i in coef_dict:
        if i == "Intercept":
            # for the Intercept
            coef = coef_dict[i]
            x.append(str(coef))
        elif i == 'lhs':
            funct = coef_dict[i][0]
        elif len(i.split(':')) > 1:
            # for the interaction term
            y = []
            for j in i.split(":"):
                y.append(multiplied_vals_interaction(var_string=j, i=i, df=df, coef_dict=coef_dict)[0])
                interaction_coef = str(multiplied_vals_interaction(var_string=j, i=i, df=df, coef_dict=coef_dict)[1])
            y.append(interaction_coef)
            x.append('*'.join(y))
        else:
            # for the non interaction terms
            x.append(multiplied_vals(var_string=i, df=df, coef_dict=coef_dict))
    prediction_formula = ' + '.join(x)
    if funct == [1]:
        prediction_formula = prediction_formula
    elif funct == '[np.log]':
        prediction_formula = "np.exp(" + prediction_formula + ")"
    elif funct == '[np.log10]':
        prediction_formula = "10 ** (" + prediction_formula + ")"
    elif funct == '[np.log2]':
        prediction_formula = "np.exp2(" + prediction_formula + ")"
    elif funct == '[np.sqrt]':
        prediction_formula = "np.square(" + prediction_formula + ")"
    elif funct == '[np.square]':
        prediction_formula = "np.sqrt(" + prediction_formula + ")"
    elif funct == '[np.negative]':
        prediction_formula = "-(" + prediction_formula + ")"

    #todo add other transformations

    if disp:
        print('The prediction formula for the regression is {}\n'.format(prediction_formula))

    return prediction_formula


######################################################################################################################
# --------------------- COEFFICIENTS --------------------------------------------------------------------------------#
######################################################################################################################

def clean_coefficients(coefficients):
    """

    @return: returns cleaned coefficients
    """
    for i in coefficients.keys():
        if i.count("[") > 0:
            coefficients[dummy_var_string_replace(i)] = coefficients[i]
            del coefficients[i]
    return coefficients


def dummy_var_string_replace(a):
    """

    @param a: the dictionary to replace the unwanted strings
    @return: dictionary with the cleaned strings
    """
    a = a.replace("[T.", "_")
    a = a.replace("]", "")
    return a


def convert_dict_to_df(dict_to, column_names, product_name, x_dict, y_dict):
    df = pd.DataFrame.from_dict(dict_to).reset_index()
    df.columns = column_names
    df['time_stamp'] = datetime.now()
    df['product_name'] = product_name
    df['rhs'] = df['variable'].map(x_dict)
    df['rhs'] = df['rhs'].fillna('[1]')
    df['lhs'] = y_dict.values()[0][0]
    df['lhs'] ='[' + df['lhs'].astype(str) + ']'
    return df


def convert_df_to_dict(df, key_column, value_column):
    keys = list(df[key_column])
    values = list(df[value_column])
    dictionary = dict(zip(keys, values))
    return dictionary


def get_mape(actuals, predictions):
    # TODO: write the code if the actual are zeros
    actuals = np.array(actuals)
    predictions = np.array(predictions)

    mape_row = abs(actuals - predictions) / actuals
    mape_overall = np.nanmean(abs(actuals - predictions) / actuals)
    return np.array(mape_row), mape_overall


def extract(d, keys):
    return dict((k, d[k]) for k in keys if k in d)


